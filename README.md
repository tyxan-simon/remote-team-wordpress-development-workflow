## Description
A standardised development workflow concept for remote teams using Git & Gulp to create synchronised local, staging and live Wordpress websites.

## Initial setup
1. Setup your staging and/or live environment(s).
2. Create your project's Git repository
3. Commit and push your staging site's code to your project's Git repository
4. Create the Trello board (for project management)
5. Use Git to pull in the staging site files to use for your local development site
6. Export your staging site's SQL database
7. Enable local development with Apache and MySQL (if you haven't already)
8. Create your local development environment, using the pulled Git files and exported SQL file
  * Make sure you use the same database login credentials as your staging site so you don't have to change your wp-config.php
  * Update the database options table fields to work with your local hostname/folder structure
9. Start developing locally!

## Before we get started..
In order for this workflow to actually work, you'll need to make sure that you have Node.js, NPM and Gulp installed on your local machine.

### Install Node.js, NPM and Gulp
1. Install node.js (https://nodejs.org/en/download/)
  * To check installation, paste `node --version` (or `node -v`) in terminal/cmd - You should see something like `v0.10.25`
2. Install npm (node package manager)
  ```
  npm install npm -g
  ```
3. Install core gulp package
  ```
  npm install gulp -g
  ```

Now you have node, npm and your basic gulp files stored on your local computer, you can start importing node packages to be used by Gulp.

### Install Gulp packages for your project (used by this example)
Most gulp packages are just node packages wrapped in gulp-friendly code. Therefore when we install gulp packages, lots of dependencies are usually installed as well.

Gulp packages are prefixed with 'gulp-'. You'll see in this example that we're using a couple of regular node packages in addition to our gulp packages.

**IMPORTANT**: Install all packages with -g (e.g. `sudo npm install gulp -g`). This installs all your packages in one global directory rather than on a per-project basis.

Once a package is installed globally, you can access it within your project using the nls package, which simply creates a link from your local directory to your global package root.
  **For example:** `npm install gulp-sass -g` (Meaning: [NODE PACKAGE MANAGER] - [INSTALL] - [GULP-SASS PACKAGE] - [GLOBALLY])

`npm install gulp-load-plugins nls -g`

This will reduce computer storage and also save you time now and in the future when you create a project that will use the same packages.

```
nls gulp gulp-load-plugins
```

You now have the basic gulp installation, it's time to install your favourite packages using `npm install <PACKAGES> -g`
The remaining packages used in this example are as follows (paste them in place of <PACKAGES> above like so):

```
npm install del gulp-autoprefixer gulp-batch-replace gulp-changed gulp-concat gulp-concat-css gulp-cssnano gulp-rename gulp-sass gulp-uglify -g
```

We also have some dependencies that we want to install so we can minify them with our project CSS, JS, IMAGES and FONTS

```
npm install @fortawesome/fontawesome-free bootstrap jquery jquery.scrollto -g
```
## Now you're ready!

Check out the gulpfile-sample.js to see how we can use gulp to manipulate our code to make development quicker and easier.

### Setup your folder structure (This bit's already done for you.. but just so you know)

This example is based around the following project folder structure:

  *  /dependencies
  *  /node_modules
  *  /src (working directory)
    *  /child-theme
    *  /css
    *  /fonts
    *  /js
    *  /scss
    *  functions.php
    *  screenshot.png
    *  style.css
  *  /wordpress (production directory)

Any questions, concerns, bugs, etc, just let me know!
