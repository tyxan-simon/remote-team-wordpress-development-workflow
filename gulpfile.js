(function(){'use strict';}());

// CONFIGURATION

var gulp = require( 'gulp' ),
    packages = require( 'gulp-load-plugins' ),
    $ = packages( {
      pattern : '*',
      rename : {
        'del'                 : 'delete',
        'gulp-autoprefixer'   : 'autoprefix',
        'gulp-batch-replace'  : 'replace',
        'gulp-concat-css'     : 'concatCSS',
        'gulp-cssnano'        : 'minifyCSS',
        'gulp-concat'         : 'concatJS',
        'gulp-uglify'         : 'minifyJS'
      },
      replaceString : /^gulp(-|\.)/
    } ),
    working = './src/',
    production = './wordpress/',
    dependencies = './dependencies/',
    theme = 'child-theme/',
    config = {
      prod : production + 'wp-content/themes/' + theme,
      dev : {
        root : working + theme + '/',
        sass : working + theme + 'scss/',
        css : working + theme + 'css/',
        js : working + theme + 'js/',
        fonts : working + theme + 'fonts/',
        images : working + theme + 'images/',
        other : [
          working + theme + '**.*',
          '!' + working + theme + 'scss/*.*',
          '!' + working + theme + 'css/*.*',
          '!' + working + theme + 'js/*.*',
          '!' + working + theme + 'fonts/*.*'
        ]
      },
      deps : {
        css : dependencies + 'css/',
        js : dependencies + 'js/'
      },
      dependencies: {
        css : [
          './node_modules/bootstrap/dist/css/bootstrap.css',
          './node_modules/@fortawesome/fontawesome-free/css/all.css'
        ],
        js : [
          './node_modules/jquery/dist/jquery.min.js',
          './node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
          './node_modules/@fortawesome/fontawesome-free/js/all.js',
          './node_modules/jquery.scrollto/jquery.scrollTo.js'
        ],
        fonts : './node_modules/@fortawesome/fontawesome-free/webfonts/*'
      }
    };

function prodRefresh() {
  return $.delete( config.prod + '**/*' );
}

// DEPENDENCIES

function depsCSS() {
  return gulp.src( config.dependencies.css )
    .pipe($.changed( config.deps.css ))
    .pipe(gulp.dest( config.deps.css ));
}
function depsJS() {
 return gulp.src( config.dependencies.js )
   .pipe($.changed( config.deps.js ))
   .pipe(gulp.dest( config.deps.js ));
}
function depsFonts() {
 return gulp.src( config.dependencies.fonts )
   .pipe($.changed( config.prod + 'fonts/' ))
   .pipe(gulp.dest( config.prod + 'fonts/' ));
}

// SASS/CSS

function compileSASS() {
  return gulp.src( config.dev.sass + 'styles.scss' )
    .pipe($.autoprefix( {
      browsers: ['last 2 versions']
    }))
    .pipe($.sass( { outputStyle: 'compressed' } ))
    .pipe(gulp.dest( config.dev.css ));
}

function gatherCSS() {
  return gulp.src( config.dependencies.css )
    .pipe($.changed( config.deps.css ))
    .pipe(gulp.dest( config.deps.css ));
}

function fixBootstrapCSS() {
  return gulp.src( config.deps.css + 'bootstrap.css' )
    .pipe($.replace( [
      [/(?="data:image)(.*)(?=\))/g, ''],
      [/(\/\*[\w\'\s\r\n\*]*\*\/)|(\/\/[\w\s\']*)|(\<![\-\-\s\w\>\/]*\>)/g, '']
    ] ))
    .pipe(gulp.dest( config.deps.css ));
}

function combineCSS() {
  return gulp.src( [
      config.deps.css + '*.css',
      config.dev.css + '*.css'
    ] )
    .pipe($.concatCSS( 'styles.min.css' ))
    .pipe($.minifyCSS())
    .pipe(gulp.dest( config.prod + 'css/' ));
}

// JAVASCRIPT

function compileJS() {
  return gulp.src( config.dev.js + '*.js', {
    base : config.dev.root
  })
    .pipe($.concatJS( 'scripts.min.js' ))
    .pipe($.minifyJS())
    .pipe(gulp.dest( config.prod + 'js/' ))
}

// OTHER FILES & DIRECTORIES

function moveFiles() {
  return gulp.src( config.dev.other, {
    allowEmpty : true
  })
    .pipe($.changed( config.prod ))
    .pipe(gulp.dest( config.prod ));
}

function watchForChanges() {
  gulp.watch( config.dev.sass + '**/*', css);
  gulp.watch( config.dev.js + '**/*', js);
  gulp.watch( config.dev.other, dist);
}

var refresh = prodRefresh;
var dependencies = gulp.parallel( depsCSS, depsJS, depsFonts );
var css = gulp.series( compileSASS, gatherCSS, fixBootstrapCSS, combineCSS );
var js = gulp.series( compileJS );
var files = moveFiles;
var dist = gulp.series( prodRefresh, dependencies, css, js, files, watchForChanges );

exports.refresh = refresh;
exports.dependencies = dependencies;
exports.css = css;
exports.js = js;
exports.files = files;
exports.default = dist;
