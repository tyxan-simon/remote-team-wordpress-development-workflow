
/**
 * - PROJECT FOLDER STRUCTURE --------------------------------------------------
 * This example is based around the following project folder structure:
 project-folder
  |_ dependencies/
  |_ node_modules/
  |_ src/ (working directory)
    |_ child-theme/
      |_ css/
      |_ fonts/
      |_ js/
      |_ scss/
      |_ functions.php
      |_ screenshot.png
      |_ style.css
  |_ wordpress/ (production directory)
*/

/**
 * - GULPFILE FOR WORDPRESS CHILD THEMES ---------------------------------------
 *    Gulp is a Node.js package that leverages the power of other packages to
      perform automated tasks to improve the speed and quality of your code.
 *    The gulpfile.js file is a gulp requirement and is used to configure your
      Gulp package settings.
 *    This file is a basic configuration that has everything you need to compile
      and optimise a Wordpress child theme.
 * - BASIC NODE, NODE PACKAGE MANAGER & GULP SETUP -----------------------------
 *    Install node.js (https://nodejs.org/en/download/)
 *      To check installation, paste "node --version" (or "node -v") in
        terminal/cmd - You should see something like "v0.10.25"
 *    Install npm (node package manager)
 *      - npm install npm -g
 *    Install core gulp package
 *      - npm install gulp -g
 *    Now you have node, npm and your basic gulp files stored on your local
      computer, you can start importing node packages to be used by Gulp.
 * - INSTALLING GULP PACKAGES (USED BY THIS EXAMPLE) ---------------------------
 *    Most gulp packages are just node packages wrapped in gulp-friendly code.
      Therefore when we install gulp packages, lots of dependencies are usually
      installed as well.
 *    Gulp packages are prefixed with 'gulp-'. You'll see in this example that
      we're using a couple of regular node packages in addition to our gulp packages.
 *    IMPORTANT: Install all packages with -g (e.g. sudo npm install gulp -g).
      This installs all your packages in one global directory rather than on a
      per-project basis.
 *    Once a package is installed globally, you can access it within your
      project using the nls package, which simply creates a link from your local
      directory to your global package root.
 *      For example: npm install gulp-sass -g (Meaning: [NODE PACKAGE MANAGER]
        - [INSTALL] - [GULP-SASS PACKAGE] - [GLOBALLY])
 *      - npm install gulp-load-plugins nls -g
 *    This will reduce computer storage and also save you time now and in the
      future when you create a project that will use the same packages.
 *      - nls gulp gulp-load-plugins
 *    You now have the basic gulp installation, it's time to install your
      favourite packages using "npm install <PACKAGES> -g"
 *    The remaining packages used in this example are as follows (paste them in
      place of <PACKAGES> above):
 *      - del gulp-autoprefixer gulp-batch-replace gulp-changed gulp-concat gulp-concat-css
      gulp-cssnano gulp-rename gulp-sass gulp-uglify
 *    We also have some dependencies that we want to install so we can minify
      them with our project CSS, JS, IMAGES and FONTS
 *      - @fortawesome/fontawesome-free bootstrap jquery jquery.scrollto
 */

// Recommended by gulp developers
(function(){'use strict';}());

/**
 * - LOAD PROJECT PACKAGES -----------------------------------------------------
 * Use gulp and the gulp-load-plugins packages to load and configure the project's node packages.
 */

// Include the main gulp package
var gulp = require( 'gulp' );
// Include gulp-load-plugins package so we don't have to manually require each of our packages
var packages = require( 'gulp-load-plugins' );
// Set gulp-load-plugins package configuration
var $ = packages( {
  // Include all plugin types, not just 'gulp-' packages
  pattern : '*',
  // Use easier naming convention for our packages
  rename : {
    'del'                 : 'delete',
    'gulp-autoprefixer'   : 'autoprefix',
    'gulp-batch-replace'  : 'replace',
    'gulp-concat-css'     : 'concatCSS',
    'gulp-cssnano'        : 'minifyCSS',
    'gulp-concat'         : 'concatJS',
    'gulp-uglify'         : 'minifyJS'
  },
  // Remove 'gulp-' from package names (E.G. "gulp-sass" becomes "sass")
  replaceString : /^gulp(-|\.)/
} );

/**
 * PROJECT CONFIGURATIONS ------------------------------------------------------
 * Set the variables of this project to make sure that gulp has access to all the directories and files it needs.
 */

// Your working directory (raw theme files)
var working = './src/';
// Your production theme directory (for deployment)
var production = './wordpress/';
// Your dependencies directory
var dependencies = './dependencies/';
// Set your child theme directory
var theme = 'child-theme/';
// Set project directories and dependencies
var config = {
  // Set the path to your production directory
  prod : production + 'wp-content/themes/' + theme,
  // Set the paths for your working files/directories
  dev : {
    // Set the root for your working directory
    root : working + theme + '/',
    // Get all SCSS files for processing
    sass : working + theme + 'scss/',
    // Get all CSS files for processing
    css : working + theme + 'css/',
    // Get all javascript files for processing
    js : working + theme + 'js/',
    // Get all font files for processing
    fonts : working + theme + 'fonts/',
    // Get all image files for processing
    images : working + theme + 'images/',
    // Get all non CSS/SCSS/JS files (php, images, fonts, etc.) for cloning
    other : [
      working + theme + '**.*',
      '!' + working + theme + 'scss/*.*',
      '!' + working + theme + 'css/*.*',
      '!' + working + theme + 'js/*.*',
      '!' + working + theme + 'fonts/*.*'
    ]
  },
  // Set the paths for your dependency files/directories
  deps : {
    // Set the css directory
    css : dependencies + 'css/',
    // Set the js directory
    js : dependencies + 'js/'
  },
  // Include all the dependencies you want to include in your project
  // Remember: You'll need to use nls to make sure your dependencies are linked in the ./node_modules/ directory.
  dependencies: {
    css : [
      './node_modules/bootstrap/dist/css/bootstrap.css',
      './node_modules/@fortawesome/fontawesome-free/css/all.css'
    ],
    js : [
      './node_modules/jquery/dist/jquery.min.js',
      './node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
      './node_modules/@fortawesome/fontawesome-free/js/all.js',
      './node_modules/jquery.scrollto/jquery.scrollTo.js'
    ],
    fonts : './node_modules/@fortawesome/fontawesome-free/webfonts/*'
  }
};

/**
 * - CREATE GULP FUNCTIONS -----------------------------------------------------
 * Start using your gulp packages to process your working code and compile it to your production folder.
 */

// Refresh production theme folder
function prodRefresh() {
  return $.delete( config.prod + '**/*' );
}

/**
 * - COPY DEPENDENCIES --------------------------------------------------------------
 * Move all declared dependencies to your dependencies directory
    a. Changed will only copy over dependency files that have changed since the our last gulp
 */
function depsCSS() {
  return gulp.src( config.dependencies.css )
    .pipe($.changed( config.deps.css )) // a.
    .pipe(gulp.dest( config.deps.css ));
}
function depsJS() {
 return gulp.src( config.dependencies.js )
   .pipe($.changed( config.deps.js ))
   .pipe(gulp.dest( config.deps.js ));
}
function depsFonts() {
 return gulp.src( config.dependencies.fonts )
   .pipe($.changed( config.prod + 'fonts/' ))
   .pipe(gulp.dest( config.prod + 'fonts/' ));
}

/**
 * - SASS/CSS ------------------------------------------------------------------
 * Process the SASS files and move the output to the production CSS folder
 */

/**
  Compile our working SASS files into a nice, clean .CSS file and push it to our working CSS folder
   a. Set the location of your main styles.scss file (the file were you include all your segmented .scss files)
   b. Use the autoprefixer package to autoprefix our sass files
   c. Compile our SASS to CSS in the compact CSS style
   d. Set the destination to push our outputted CSS file to
 */
function compileSASS() {
  return gulp.src( config.dev.sass + 'styles.scss' ) // a.
    .pipe($.autoprefix( { // b.
      browsers: ['last 2 versions']
    }))
    .pipe($.sass( { outputStyle: 'compressed' } )) // c.
    .pipe(gulp.dest( config.dev.css )); // d.
}

/**
  Gather all our dependencies CSS files and put them in our working CSS folder
 */
function gatherCSS() {
  return gulp.src( config.dependencies.css )
    .pipe($.changed( config.deps.css ))
    .pipe(gulp.dest( config.deps.css ));
}

/**
  Remove SVG images from bootstrap.css that breaks our CSS concact function
    a. Using gulp-batch-replace (renamed to replace), we can use regex to remove the troublesome code
 */
function fixBootstrapCSS() {
  return gulp.src( config.deps.css + 'bootstrap.css' )
    .pipe($.replace( [ // a.
      [/(?="data:image)(.*)(?=\))/g, ''],
      [/(\/\*[\w\'\s\r\n\*]*\*\/)|(\/\/[\w\s\']*)|(\<![\-\-\s\w\>\/]*\>)/g, '']
    ] ))
    .pipe(gulp.dest( config.deps.css ));
}

/**
  Concatinate all .CSS files in our working CSS directory into a single file and push it to our production CSS directory
    a. Set the filename of your concatinated CSS file
 */
function combineCSS() {
  return gulp.src( [
      config.deps.css + '*.css',
      config.dev.css + '*.css'
    ] )
    .pipe($.concatCSS( 'styles.min.css' )) // a.
    .pipe($.minifyCSS())
    .pipe(gulp.dest( config.prod + 'css/' ));
}

/* - JAVASCRIPT ----------------------------------------------------------------
 * Gather our JS dependencies, working JS file(s) and compile them to our production JS folder
 */

/**
  Optimise and combine all files in your working JS directory and move a single JS file to our production JS folder
    a. As we're copying over directories, we need to set a mutual root directory between our working and production directories
    b. Combine all JS files within your working JS directory into a single file
    c. Minify your compiled JS file before moving it to production
 */
function compileJS() {
  return gulp.src( config.dev.js + '*.js', {
    base : config.dev.root // a.
  })
    .pipe($.concatJS( 'scripts.min.js' )) // b.
    .pipe($.minifyJS()) // c.
    .pipe(gulp.dest( config.prod + 'js/' ))
}

/**
 * - OTHER FILES & DIRECTORIES ----------------------------------------------------------------
 * Copy over remaining files (excluding fonts, .css, .scss and .js files which have already been processed and copied over)
  a. Allowing empty will make sure that if no other files are found, gulp won't throw us an error for making an invalid request
 */
function moveFiles() {
  return gulp.src( config.dev.other, {
    allowEmpty : true // a.
  })
    .pipe($.changed( config.prod ))
    .pipe(gulp.dest( config.prod ));
}

/**
 * - GULP WATCHERS ------------------------------------------------------------
 * Gulp watchers will detect changes to your files and automatically carry out your functions!
 */
function watchForChanges() {
  gulp.watch( config.dev.sass + '**/*', css);
  gulp.watch( config.dev.js + '**/*', js);
  gulp.watch( config.dev.other, dist);
}

/**
 * - GULP! ------------------------------------------------------------
 * Using all the functions above, we can now combine them together in a series identified by a descriptive global
    a. Execution types: series(..) will perform functions in a sequence whereas parallel(..) will perform all indicated functions at once
 */

var refresh = prodRefresh;
var dependencies = gulp.parallel( depsCSS, depsJS, depsFonts ); // a.
var css = gulp.series( compileSASS, gatherCSS, fixBootstrapCSS, combineCSS );
var js = gulp.series( compileJS );
var files = moveFiles;
var dist = gulp.series( prodRefresh, dependencies, css, js, files, watchForChanges );

/**
 * - GULP EXPORTS ------------------------------------------------------------
 * In terminal/cmd, you can use the below to manually perform only certain functions.
   By default, you would just type 'gulp'. However, if you types 'gulp-refresh', it would perform the prod_refresh function above.
 */
exports.refresh = refresh;
exports.dependencies = dependencies;
exports.css = css;
exports.js = js;
exports.files = files;
exports.default = dist;
